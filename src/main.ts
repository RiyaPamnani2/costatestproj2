import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import {config} from "dotenv";

async function bootstrap() {
  config();
  const app = await NestFactory.create(AppModule);
  await app.listen(process.env.PORT || 80);
  console.log(`Application is running on: ${await app.getUrl()}`);
}
bootstrap(); 